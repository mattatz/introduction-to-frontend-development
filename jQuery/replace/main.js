/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    var listB = $("#listB");

    var newList = $("<li>new list</li>");

    // listBを<li>new list</li>で置き換える
    listB.replaceWith(newList);

});

