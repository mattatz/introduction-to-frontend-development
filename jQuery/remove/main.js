/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    var testDiv1 = $("#testDiv1");
    testDiv1.remove();

    var testDiv2 = $("#testDiv2");
    testDiv2.empty();

});

