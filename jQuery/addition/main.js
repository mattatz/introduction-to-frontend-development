/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    var ul = $("ul"); // index.html内のul要素を選択

    var D = $("<li>D</li>"); // <li>D</li>というHTML要素を生成
    var E = $("<li>E</li>");
    var header = $("<p>header</p>");
    var footer = $("<p>footer</p>");

    ul.append(D);       // ulの最後にDを追加
    ul.prepend(E);      // ulの先頭にEを追加
    ul.before(header);  // ulの直前にheaderを追加
    ul.after(footer);   // ulの直後にfooterを追加

});

