/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    // $("#id名")でdocument.getElementByIdと同じ要素の取得を行う
    var parentDiv = $("#parentDiv");

    var children = parentDiv.children(); // 子要素すべて
    console.log(children);

    var ps = parentDiv.children("p"); // p要素すべて
    console.log(ps);

    var boldps = parentDiv.children("p.bolder"); // p.bolder要素すべて
    console.log(boldps);

    var testP = parentDiv.children("#testP"); // p#testP要素
    console.log(testP);

    var img = parentDiv.children("img"); // img要素
    console.log(img);

});

