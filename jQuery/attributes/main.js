/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    var div1 = $("#testDiv1");
    var div2 = $("#testDiv2");
    var div3 = $("#testDiv3");

    // .test1クラスを外して.test2クラスに
    div1.removeClass("test1");
    div1.addClass("test2");

    // .test2クラスを外して.test3クラスに
    div2.removeClass("test2");
    div2.addClass("test3");

    // .test3クラスを外して.test1クラスに
    div3.removeClass("test3");
    div3.addClass("test1");

    var img = $("#testImg");

    // img要素のsrc属性の値をchrome.jpgで上書きする
    img.prop("src", "chrome.jpg");

});

