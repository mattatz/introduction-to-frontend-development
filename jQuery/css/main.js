/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 */
$(document).ready(function() {

    var div1 = $("#testDiv1");
    var div2 = $("#testDiv2");
    var div3 = $("#testDiv3");

    /*
     * 引数にcss property名を指定することでそのpropertyの値が取得できる
     * */
    var w = div1.css("width");
    console.log(w); // 100px

    /*
     * 第一引数にproperty名
     * 第二引数に値を指定することで
     * そのcss propertyの値を変更できる
     * */
    div1.css("width", "200px");

    /*
     * 引数に連想配列を指定することで
     * 複数のcss propertyを一度に変更できる
     * */
    div2.css({
        width : "300px",
        backgroundColor : "rgba(255, 255, 0, 1.0)",
        border: "5px solid black"
    });

});

