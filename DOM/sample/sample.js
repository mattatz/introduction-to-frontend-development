/*
 * sample.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {
    console.log(document.childNodes);
    console.log(document.firstChild.childNodes);

    // htmlノードの子要素の中の最後のノードであるbodyノードを取得
    // var body = document.firstChild.lastChild;
    var nodes = document.firstChild.childNodes;
    var body = document.firstChild.childNodes[nodes.length - 1];
    
    console.log(body);
    console.log(body.childNodes);
};


