/*
 * gulpfile.js
 * */

var gulp = require("gulp");
var ejs = require("gulp-ejs");

gulp.task("ejs", function() {
    // ./templates/*.ejsファイルは読みこむが
    // ./templates/common/*_.ejsファイルは読み込まない
    gulp.src(["./templates/*.ejs", "!./templates/common/_*.ejs"])
        .pipe(ejs({}))
        .pipe(gulp.dest("./html"));
});



