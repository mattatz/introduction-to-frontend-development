/*
 * gulpfile.js
 * */

var fs = require("fs");
var gulp = require("gulp");
var ejs = require("gulp-ejs");

gulp.task("ejs", function() {

    var json = JSON.parse(fs.readFileSync("./json/index.json"));

    gulp.src("./templates/*.ejs")
        .pipe(ejs(json))
        .pipe(gulp.dest("./html"));

});



