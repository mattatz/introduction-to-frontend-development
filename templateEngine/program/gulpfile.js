/*
 * gulpfile.js
 * */

var gulp = require("gulp");
var ejs = require("gulp-ejs");

gulp.task("ejs", function() {

    gulp.src("./templates/*.ejs")
        .pipe(ejs({
            title : "title変数の値"
        }))
        .pipe(gulp.dest("./html"));

});



