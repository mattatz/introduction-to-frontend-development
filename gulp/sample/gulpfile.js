
var gulp = require("gulp");

// watchタスクの定義
gulp.task("watch", function() {
    // ファイルが変更されたらコンソールにメッセージを表示
    gulp.watch("*", function(event) {
        console.log("ファイル " + event.path + " " + event.type);
    });
});

// defaultタスクに上述のwatchタスクを登録
gulp.task("default", ["watch"]);


