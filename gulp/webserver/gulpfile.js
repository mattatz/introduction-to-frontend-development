
var gulp = require("gulp");

var webserver = require("gulp-webserver");

gulp.task("webserver", function() {

    gulp.src("")            // Webサーバで表示するサイトのルートディレクトリを指定
        .pipe(webserver({
            livereload : true,  // ライブリロードを有効にする
            directoryListing : true // ディレクトリ一覧を表示する
        }));

});

