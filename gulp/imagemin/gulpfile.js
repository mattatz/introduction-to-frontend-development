
var gulp = require("gulp");

var imagemin = require("gulp-imagemin"); // gulp-imageminプラグインを読み込み

// imageminという名前のタスクを定義
gulp.task("imagemin", function() {

    gulp.src("images/*.png")    // imagesフォルダ以下のpng画像を取得
        .pipe(imagemin())       // 画像の圧縮処理を実行
        .pipe(gulp.dest("minified_images/")); // minified_imagesフォルダ以下に圧縮した画像を保存

});

