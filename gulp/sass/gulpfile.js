
var gulp = require("gulp");

var sass = require("gulp-sass"); // gulp-sassプラグインを読み込み

// sassという名前のタスクを定義
gulp.task("sass", function() {

    gulp.src("sass/*.scss")     // sassフォルダ以下のscssを取得
        .pipe(sass())           // cssへの変換を実行
        // .pipe(sass({ outputStyle : "compressed" })) // cssの圧縮までやる場合はこっち
        .pipe(gulp.dest("css")); // cssフォルダ以下に変換したcssを保存

});

gulp.task("watch", function() {

    // sassフォルダ以下のscssファイルが変更されたら
    // 上で定義しているsassタスクを実行する
    gulp.watch("sass/*.scss", ["sass"]);

});

