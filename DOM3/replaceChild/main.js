/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    var parentDiv = document.getElementById("parentDiv");

    var divChildren = parentDiv.getElementsByTagName("div");

    // 新しい要素の作成
    var newElement = document.createElement("div");

    // .classNameのプロパティを変更することで要素のクラス名を変更できる
    newElement.className = "block"; 

    newElement.innerHTML = "<p>testDiv3</p>";

    // 要素の置き換え
    parentDiv.replaceChild(newElement, divChildren[0]);

};

