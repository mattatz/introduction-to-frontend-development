/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    // 削除したい要素の親要素を取得
    var parentDiv = document.getElementById("parentDiv");

    // parentDivの子要素内のdiv要素を取得
    var divChildren = parentDiv.getElementsByTagName("div");

    // 削除
    parentDiv.removeChild(divChildren[0]);

};

