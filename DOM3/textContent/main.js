/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    // id#testのp要素を取得
    var elm = document.getElementById("test");

    // .textContentプロパティを書き換えることでテキスト内容を変更できる
    elm.textContent = "書き換え後のテキスト";

};

