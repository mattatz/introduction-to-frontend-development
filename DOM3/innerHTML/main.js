/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    // id#testのp要素を取得
    var elm = document.getElementById("test");

    // .innerHTMLプロパティを書き換えることでHTMLを変更できる
    elm.innerHTML = "<b>書き換え</b>後の<i>HTML</i>";

    var div = document.getElementById("testDiv");
    div.innerHTML = 
        "<ul>" + 
            "<li>" + 
                "<p>追加したリスト1</p>" + 
            "</li>" + 
            "<li>" + 
                "<p>追加したリスト2</p>" + 
            "</li>" + 
        "</ul>";

};

