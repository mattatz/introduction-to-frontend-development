/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    // createElement関数でp要素を作成
    var elm = document.createElement("p");

    // .textContentの内容を書き換えることでテキストを追加
    elm.textContent = "新しく作成したp要素";

    // elmを追加したい階層の一つ上に位置する親要素
    var parentDiv = document.getElementById("parentDiv");

    // elmを追加したい位置の前にある要素
    var div1 = document.getElementById("testDiv1");

    parentDiv.insertBefore(elm, div1);

};

