/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    // div id="test"の要素を取得する
    var testDOM = document.getElementById("test");

    // div id="test"の要素の背景色を変更する
    testDOM.style.backgroundColor = "rgba(0, 255, 0, 1)"; // 緑にする
    // testDOM.style.backgroundColor = "rgba(0, 0, 255, 1)"; // 青にする

    // div id="test"の要素のp子要素を取得する
    var p = testDOM.getElementsByTagName("p")[0];
    
    // p要素のフォントの大きさや太さを変更する
    p.style.fontSize = "30px";
    p.style.fontWeight = "bold";


};

