/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    var button = document.getElementById("testBtn");

    button.addEventListener("click", function() {
        console.log("クリックされた1");
    });

    button.addEventListener("click", function() {
        console.log("クリックされた2");
    });

};

