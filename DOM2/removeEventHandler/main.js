/*
 * main.js
 * */

/*
 * ページが読み込まれた時に実行される関数
 * window.onload
 * */
window.onload = function() {

    var button = document.getElementById("testBtn");

    var handler = function() {
        alert("クリックされた");

        button.removeEventListener("click", handler); // イベントハンドラの削除
    };

    button.addEventListener("click", handler);


};

