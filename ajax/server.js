/*
 * server.js
 * */

var express = require("express");

var app = express();

app.use(express.static("public"));

// ------ ajax settings

app.get("/ajax/sample", function(req, res) {
    res.send("message from server...");
});

app.get("/ajax/json", function(req, res) {

    res.send(JSON.stringify({
        message : "サーバからの文字列",
        message2 : "サーバからの文字列2",
        message3 : "サーバからの文字列3"
    }));

});

app.get("/ajax/createHTML", function(req, res) {

    var people = [
        {
            name : "Mike",
            age : "19"
        },
        {
            name : "Alice",
            age : "14"
        },
        {
            name : "Bob",
            age : "32"
        }
    ];

    res.send(JSON.stringify(people));

});

// ajax settings ------ 

app.listen(8080);

