/*
 * main.js
 * */

function request() {

    $.ajax({
        url : "http://localhost:8080/ajax/createHTML",
        method : "GET", 
        success : function(responseText) { // 通信成功時に呼ばれる関数

            var people = JSON.parse(responseText);

            var $ul = $("#people_list");
            for(var i = 0; i < people.length; i++) {
                var person = people[i];

                console.log(person);

                var $li = $(
                    "<li>" + 
                        "<h2>" + 
                            person.name + 
                        "</h2>" + 
                        "<p>" + 
                            person.age + "歳" + 
                        "</p>" + 
                    "</li>"
                );

                $ul.append($li);
            }

        },
        error : function(e) {} // 通信失敗時に呼ばれる関数
    });

}

// entry point
window.onload = function() {

    var $button = $("#people_button");
    $button.on("click", function() {
        request();
    });

};

