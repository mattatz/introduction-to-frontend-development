/*
 * main.js
 * */

// entry point
window.onload = function() {
    var req = new XMLHttpRequest();

    // XMLHttpRequestオブジェクトの状態が変化した時に実行される関数
    req.onreadystatechange = function(evt) {
        /*
         * readyStateプロパティの値をチェックすることで
         * 通信がどのような状態か判別することができる
         * */
        switch(req.readyState) { // 0 ~ 4の数値が含まれている
            case 0:
                break;
            case 1: // send()関数がまだ呼び出されていない
                break;
            case 2: // send()関数が呼び出された
                break;
            case 3: // 通信中
                break;
            case 4: // 通信の終了
                // responseTextプロパティにサーバから送られた文字列が含まれている
                console.log(req.responseText);
                break;
        }
    };

    // 通信先の設定
    req.open("GET", "http://localhost:8080/ajax/sample");

    // 通信の送信
    req.send();
};

